import { root } from './index';

const introSelector = '.App-header > span';
const marketBarSelector = '.market-bar';
const tableSelector = 'table';
const switchSelector = '.app-switch';

export const getIntroText = async () => {
	const app = await root();
	return await app.$eval(introSelector, (el) => el.innerText);
};

export const getMarketBar = async () => {
	const app = await root();
	return await app.$eval(marketBarSelector, (el) => el.className);
};

export const getSwitch = async () => {
	const app = await root();
	return await app.$eval(switchSelector, (el) => el.className);
};

export const getTable = async () => {
	const app = await root();
	return await app.$eval(tableSelector, (el) => el.className);
};
