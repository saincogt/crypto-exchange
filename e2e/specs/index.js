import { load, getTitle } from '../pageObjects/index';

describe('Corret Title', () => {
	it(`should be titled Sancho's Crypto Exchange Market Widget`, async () => {
		await load();
		expect(await getTitle()).toBe(`Sancho's Crypto Exchange Market Widget`);
	});
});
