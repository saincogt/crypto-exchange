import { getIntroText, getMarketBar, getSwitch, getTable } from '../pageObjects/app';
import { load } from '../pageObjects/index';

describe('Crypto Exchange Widget React App', () => {
	beforeEach(async () => {
		await load();
	});

	it('should show the correct intro', async () => {
		expect(await getIntroText()).toBe(
			`Sancho's Crypto Exchange Widget`
		);
	});

	it('should show the switch button', async () => {
		expect(await getSwitch()).toMatch(/app-switch/);
	});

	it('should show the market selection bar', async () => {
		expect(await getMarketBar()).toMatch(/market-bar/);
	});

	it('should show a sortable table', async () => {
		expect(await getTable()).toMatch(/sortable table/);
	});
});
