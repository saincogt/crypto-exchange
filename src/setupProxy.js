const { createProxyMiddleware } = require('http-proxy-middleware');
module.exports = function (app) {
	app.use(
		'/exchange-api/v1/public/asset-service/product/get-products',
		createProxyMiddleware({
			target: 'https://www.binance.com',
			changeOrigin: true
		})
	);
};
