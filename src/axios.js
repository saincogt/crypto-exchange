import axios from 'axios';

export default axios.create({
	// baseURL: 'https://thingproxy.freeboard.io/fetch/https://www.binance.com/exchange-api/v1/public/asset-service/product/get-products',
	baseURL: '/exchange-api/v1/public/asset-service/product/get-products',
	headers: {
		'Access-Control-Allow-Origin': '*',
		'X-Requested-With': 'XMLHttpRequest'
	}
});
