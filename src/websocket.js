const ws = new WebSocket('wss://stream.binance.com/stream');
const SUBSCRIBE = JSON.stringify({'method': 'SUBSCRIBE', 'params': ['!miniTicker@arr'], 'id': 1});
const UNSUBSCRIBE = JSON.stringify({'method': 'UNSUBSCRIBE', 'params': ['!miniTicker@arr'], 'id': 1});


export { ws, SUBSCRIBE, UNSUBSCRIBE };
