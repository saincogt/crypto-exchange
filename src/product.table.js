import React from 'react';
import _ from 'lodash';
import { Table, TableHeader } from 'semantic-ui-react';
import './product.table.css';

const ProductTable = ({
	products,
	column,
	direction,
	handleSort,
	sortPercentage
}) => {
	const toNonExponential = (num) => {
		if (!num) return;
		var fNum = parseFloat(num);
		var m = fNum.toExponential().match(/\d(?:\.(\d*))?e([+-]\d+)/);
		return fNum.toFixed(Math.max(0, (m[1] || '').length - m[2]));
	};

	return (
		<Table sortable celled fixed className='market-table'>
			<TableHeader>
				<Table.Row>
					<Table.HeaderCell
						sorted={column === 's' ? direction : null}
						onClick={() => handleSort('s')}
					>
						Pair
					</Table.HeaderCell>
					<Table.HeaderCell
						sorted={column === 'c' ? direction : null}
						onClick={() => handleSort('c')}
					>
						Last Price
					</Table.HeaderCell>
					<Table.HeaderCell
						sorted={column === 'change' ? direction : null}
						onClick={() => sortPercentage('change')}
					>
						Change
					</Table.HeaderCell>
				</Table.Row>
			</TableHeader>
			<Table.Body>
				{_.map(products, ({ s, b, q, c, o }) => (
					<Table.Row key={s}>
						<Table.Cell>{`${b}/${q}`}</Table.Cell>
						<Table.Cell>{toNonExponential(c)}</Table.Cell>
						<Table.Cell>{`${c > o ? `+` : ''}${(
							(c / o - 1) *
							100
						).toFixed(2)}%`}</Table.Cell>
					</Table.Row>
				))}
			</Table.Body>
		</Table>
	);
};

export default ProductTable;
