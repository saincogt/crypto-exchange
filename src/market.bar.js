import React from 'react';
import { Dropdown, Menu } from 'semantic-ui-react';
import './market.bar.css';

const options = [
	{ key: 'ALTS', text: 'ALTS', value: 'ALTS'},
	{ key: 'XRP', text: 'XRP', value: 'XRP' },
	{ key: 'ETH', text: 'ETH', value: 'ETH' },
	{ key: 'TRX', text: 'TRX', value: 'TRX' }
];
const MarketBar = ({ handleItemClick, handleChange, activeMarket }) => {
	return (
		<Menu secondary className='market-bar'>
			<Menu.Item
				name='All'
				active={activeMarket === 'All'}
				onClick={handleItemClick}
			/>
			<Menu.Item
				name='BTC'
				active={activeMarket === 'BTC'}
				onClick={handleItemClick}
			/>
			<Menu.Item
				name='BNB'
				active={activeMarket === 'BNB'}
				onClick={handleItemClick}
			/>
			<Menu.Item
				name='USDⓈ'
				active={activeMarket === 'USDⓈ'}
				onClick={handleItemClick}
			/>
			<Dropdown name='ALTS' className={activeMarket === 'ALTS' ? 'drop-down' : ''} item placeholder='ALTS' onChange={handleChange} options={options} >
			</Dropdown>
		</Menu>
	);
};

export default MarketBar;
