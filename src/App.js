import React, { useState, useEffect, Fragment } from 'react';
import ProductTable from './product.table';
import Spinner from './spinner';
import './App.css';
import Logo from './assets/logo.svg';
import _ from 'lodash';
import MarketBar from './market.bar';
import { ws, SUBSCRIBE, UNSUBSCRIBE } from './websocket';
import Switch from 'react-switch';
import axios from './axios';
import 'semantic-ui-css/semantic.min.css'

const App = () => {
	const [state, setState] = useState({
		isConnectOpen: false,
		products: [],
		isLoading: true,
		column: null,
		direction: null
	});
	const { column, direction, isLoading, products, isConnectOpen } = state;

	const handleItemClick = (e, { name }) => {
		if (name === 'All') {
			setState({ ...state, activeMarket: name, products: state.data, column: null, direction: null});
			return;
		}
		setState({...state, activeMarket: name, column: null, direction: null, products: state.data.filter(item => item.pm === name)});
	};

	const handleChange = (e, { value, name }) => {
		setState({...state, activeMarket: name, column: null, direction: null });
		if (value === 'ALTS') {
			setState({...state, activeMarket: name, products: state.data.filter(item => item.pm === name)});
		} else {
			setState({...state, activeMarket: name, products: state.data.filter(item => item.q === value)});
		}
	};

	const handleSort = (clickedColumn) => {
		if (column !== clickedColumn) {
			setState({
				...state,
				column: clickedColumn,
				products: _.sortBy(products, [clickedColumn]),
				direction: 'ascending',
			});
			return;
		}
		setState({
			...state,
			products: products.reverse(),
			direction: direction === 'ascending' ? 'descending' : 'ascending'
		});
	};

	const sortPercentage = (clickedColumn) => {
		if (column !== clickedColumn) {
			setState({
				...state,
				column: clickedColumn,
				products: products.sort((a, b) => Math.abs((a.c / a.o - 1)) - Math.abs((b.c / b.o - 1))),
				direction: 'ascending',
			});
			return;
		}
		setState({
			...state,
			products: products.reverse(),
			direction: direction === 'ascending' ? 'descending' : 'ascending'
		});
	};

	useEffect(() => {
		const setUpWebsocket = () => {
			ws.onopen = () => {
				ws.send(SUBSCRIBE);
				setState(s => ({...s, isConnectOpen: true}))
				console.log('Connected to server');
			};
			ws.onerror = (err) => console.log(`Error with the connection: ${err}`);
			ws.onclose = () => console.log('Close the connection');
		};
		axios.get().then((response) => response.data)
			.then(resData => setState(s => ({ ...s, data: resData.data, products: resData.data, isLoading: false })))
			.catch((err) => console.log(err));
		setUpWebsocket();
		ws.onmessage = (message) => {
			const dataFromServer = JSON.parse(message.data);
			if (dataFromServer.data) {
				const data = dataFromServer.data;
				setState(s => ({
					...s,
					products: s.products.map(item => {
						let product = data.find(product => product.s === item.s);
						return product ? {...item, c: product.c, o: product.o} : { ...item }
					})
				}))
			}
		}
		return () => {
			ws.close();
			console.log('Connection Closed');
		}
	}, []);

	const switchStream = () => {
		ws.send(isConnectOpen ? UNSUBSCRIBE : SUBSCRIBE);
		console.log(isConnectOpen ? 'UNSUBSCRIBE' : 'SUBSCRIBE');
		setState({ ...state, isConnectOpen: !isConnectOpen });
	};

	return (
		<div className='App'>
			<header className='App-header'>
				<img src={Logo} className='App-logo' alt='logo' />
				<span className='header-title'>
					Sancho's Crypto Exchange Widget
				</span>
				<Switch
					className='app-switch'
					onChange={() => switchStream()}
					checked={isConnectOpen}
					height={24}
					width={48}
				/>
			</header>
			{isLoading ? <Spinner />: (
				<Fragment>
					<MarketBar
						handleItemClick={handleItemClick}
						handleChange={handleChange}
						activeMarket={state.activeMarket}
					/>
					<ProductTable
						products={products}
						column={column}
						direction={direction}
						handleSort={handleSort}
						sortPercentage={sortPercentage}
					/>
				</Fragment>
			)}
		</div>
	);
};

export default App;
